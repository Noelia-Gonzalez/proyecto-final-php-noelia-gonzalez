<?php

namespace App\Form;

use App\Entity\Opiniones;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OpinionForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('autor', null, ['label' => 'Autor']);
        $builder->add(
            'comentario',
            TextareaType::class,
            [
                'help' => 'Máximo 500 caracteres'
            ]
        );
        $builder->add('ciudad', null, ['label' => 'Ciudad']);

        $builder->add('guardar', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => Opiniones::class]);
    }
}
