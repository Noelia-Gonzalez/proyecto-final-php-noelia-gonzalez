<?php

namespace App\Controller;

use App\Form\OpinionForm;
use App\Entity\Formulario;
use App\Entity\Opiniones;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{

    /**
     * @Route("/maleteo", name="maleteo")
     */
    //Ejercicio 1
    public function index()
    {
        return $this->render('base.html.twig');
    }

    /**
     * @Route("/maleteo/formulario")
     */
    //Ejercicio 2
    public function formulario(Request $request, EntityManagerInterface $doctrine)
    {
        $nombre = $request->get('nombre');
        $email = $request->get('email');
        $ciudad = $request->get('ciudad');

        $formulario = new formulario();
        $formulario->setNombre($nombre);
        $formulario->setEmail($email);
        $formulario->setCiudad($ciudad);

        $doctrine->persist($formulario);
        $doctrine->flush();

        return $this->redirectToRoute('maleteo');
    }

    /**
     * @Route("/maleteo/opiniones")
     */
    //Ejercicio 3
    public function mostrarOpiniones(EntityManagerInterface $doctrine)
    {
        $repo = $doctrine->getRepository(Opiniones::class);
    }

    /**
     * @Route("maleteo/solicitudes")
     */
    //Ejercicio 4
    public function listarSolicitudes(EntityManagerInterface $doctrine)
    {
        $solicitudes = $doctrine->getRepository(Formulario::class)->findAll();
        return $this->render(
            'list-solicitudes.html.twig',
            [
                'formulario' => $solicitudes
            ]
        );
    }

    /**
     * @Route("maleteo/insertar-opiniones")
     */
    public function opinionManual(EntityManagerInterface $doctrine)
    {
        $opinion1 = new Opiniones();
        $opinion1->setComentario("Hola");
        $opinion1->setAutor("Noelia");
        $opinion1->setCiudad("Madrid");

        $opinion2 = new Opiniones();
        $opinion2->setComentario("Hola1");
        $opinion2->setAutor("Jorge");
        $opinion2->setCiudad("Zaragoza");

        $opinion3 = new Opiniones();
        $opinion3->setComentario("Hola2");
        $opinion3->setAutor("Pilar");
        $opinion3->setCiudad("Almería");

        $doctrine->persist($opinion1);
        $doctrine->persist($opinion2);
        $doctrine->persist($opinion3);

        $doctrine->flush();
    }


    /**
     * @Route("maleteo/formulario-opiniones")
     */
    public function opinionFormulario(Request $request, EntityManagerInterface $doctrine)
    {
        $formOpinion = $this->createForm(OpinionForm::class);

        $formOpinion->handleRequest($request);

        if ($formOpinion->isSubmitted() && $formOpinion->isValid()) {
            $data = $formOpinion->getData();
            $opinion = new Opiniones($data['comentario'], $data['autor'], $data['ciudad']);

            $opinion = $formOpinion->getData();
            $doctrine->persist($opinion);
            $doctrine->flush();
        }
        return $this->render(
            'insert-opinion.html.twig',
            [
                'OpinionForm' => $formOpinion->createView()
            ]
        );
    }
}
